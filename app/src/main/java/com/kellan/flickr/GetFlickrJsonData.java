package com.kellan.flickr;

import android.net.Uri;
import android.util.Log;

import java.util.List;

/**
 * Created by kellan on 3/30/15.
 */
public class GetFlickrJsonData extends GetRawData {

    private String LOG_TAG = GetFlickrJsonData.class.getSimpleName();
    private List<Photo> mPhotos;
    private Uri mDestinationUri;

    public GetFlickrJsonData(String searchCriteria, boolean matchAll) {
        super(null);
        createAndUpdateUri(searchCriteria, matchAll);
    }

    public boolean createAndUpdateUri(String searchCriteria, boolean matchAll) {
        final String FLICKR_API_BASE_URL = "https://api.flickr.com/services/feeds/photos_public.gne";
        final String TAGS_PARAM = "tags";
        final String TAGMODE_PARAM = "tagmode";
        final String NO_JSON_CALLBACK_PARAM = "nojsoncallback";
        final String FORMAT_PARAM = "format";


        mDestinationUri = Uri.parse(FLICKR_API_BASE_URL).buildUpon()
            .appendQueryParameter(TAGS_PARAM,searchCriteria)
            .appendQueryParameter(TAGMODE_PARAM, matchAll ? "ALL" : "ANY")
            .appendQueryParameter(FORMAT_PARAM, "json")
            .appendQueryParameter(NO_JSON_CALLBACK_PARAM, "1")
            .build();

        return mDestinationUri != null;

        }

        public void processResult() {
        if(getmDownloadStatus() != DownloadStatus.OK) {
            Log.e(LOG_TAG, "Error downloading raw json file");
            return;
        }

           final String FLICKER_ITEMS = "items";
            final String FLICKR_TITLE = "title";
            final String FLICKR_MEDIA = "media";
            final String FLICKR_PHOTO_URL = "m";
            final String FLICKR_AUTHOR = "author";
            final String FLICKR_AUTHOR_ID = "author_id";
            final String FLICKER_LINK = "link";
            final String FLICKR_TAGS = "tags";
    }

        public class DownloadJsonData extends DownloadRawData {
            protected void onPostExecute(String webData) {
                super.onPostExecute(webData);
                processResult();
            }


            protected String doInBackground(String... params) {
                return super.doInBackground(params);
            }
        }
}
